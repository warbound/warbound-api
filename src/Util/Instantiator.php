<?php

namespace App\Util;

use App\Exception\BadRequestHttpException;
use FOS\RestBundle\Request\ParamFetcherInterface;
use JMS\Serializer\ContextFactory\DeserializationContextFactoryInterface;
use JMS\Serializer\DeserializationContext;
use JMS\Serializer\SerializerInterface;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException as CoreBadRequestHttpException;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class Instantiator
{
    private $serializer;
    private $paramFetcher;
    private $contextFactory;
    private $validator;

    public function __construct(
        SerializerInterface $serializer,
        ParamFetcherInterface $paramFetcher,
        DeserializationContextFactoryInterface $contextFactory,
        ValidatorInterface $validator
    ) {
        $this->serializer = $serializer;
        $this->paramFetcher = $paramFetcher;
        $this->contextFactory = $contextFactory;
        $this->validator = $validator;
    }

    public function bodyFetch(
        string $class,
        string $group,
        object $onto = null,
        bool $validate = true
    ): object {
        $serialized = $this->paramFetcher->get('instance');
        if (!$serialized) {
            throw new CoreBadRequestHttpException('REQUEST_BODY_CANNOT_BE_EMPTY');
        }

        $context = $this->buildContext($group, $onto);

        $instance = $this->serializer->deserialize(
            $serialized,
            $class,
            'json',
            $context
        );

        if ($validate) {
            $this->validate($instance, $group);
        }

        return $instance;
    }

    private function buildContext(string $group, ?object $onto): DeserializationContext
    {
        $context = $this->contextFactory->createDeserializationContext();
        $context
            ->setGroups([$group])
            ->setAttribute('target', $onto)
        ;

        return $context;
    }

    public function validate(object $instance, string $group): void
    {
        $violations = $this->validator->validate($instance, null, $group);
        if ($violations->count() > 0) {
            throw new BadRequestHttpException($violations);
        }
    }
}

<?php

namespace App\Util;

use App\Entity\Component\InstanceList;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\EntityManagerInterface;
use FOS\RestBundle\Request\ParamFetcherInterface;
use InvalidArgumentException;
use ReflectionClass;

class Lister
{
    private $managerRegistry;
    private $paramFetcher;

    public function __construct(
        ManagerRegistry $managerRegistry,
        ParamFetcherInterface $paramFetcher
    ) {
        $this->managerRegistry = $managerRegistry;
        $this->paramFetcher = $paramFetcher;
    }

    public function list(string $class): InstanceList
    {
        $list = $this->initializeList($class);
        $manager = $this->getManager($class);

        $this->populateList($list, $manager, $class);
        $this->countTotal($list, $manager, $class);

        return $list;
    }

    private function initializeList(string $class): InstanceList
    {
        $list = new InstanceList();
        $meta = $list->getMetadata();

        $page = $this->paramFetcher->get('page');
        if ($page && $page >= 0) {
            $meta->setPage($page);
        } else {
            $meta->setPage(0);
        }

        $limit = $this->paramFetcher->get('limit');
        if ($limit && $limit >= 0) {
            $meta->setLimit($limit);
        } else {
            $meta->setLimit(20);
        }

        /** @noinspection PhpUnhandledExceptionInspection */
        $reflection = new ReflectionClass($class);
        $sort = $this->paramFetcher->get('sort');
        if ($sort && $reflection->hasProperty($sort)) {
            $meta->setSortBy($sort);
        } else {
            $field = $reflection->hasProperty('id') ? 'id' : $reflection->getProperties()[0]->getName();
            $meta->setSortBy($field);
        }

        $order = $this->paramFetcher->get('order');
        if ($order && $order === 'asc') {
            $meta->setSortOrder('asc');
        } else {
            $meta->setSortOrder('desc');
        }

        return $list;
    }

    private function getManager(string $class): EntityManagerInterface
    {
        /** @var EntityManagerInterface $manager */
        $manager = $this->managerRegistry->getManagerForClass($class);
        if (!$manager) {
            throw new InvalidArgumentException(sprintf('No entity manager found for class %s.', $class));
        }

        return $manager;
    }

    private function populateList(
        InstanceList $list,
        EntityManagerInterface $manager,
        string $class
    ): void {
        $builder = $manager->createQueryBuilder();
        $meta = $list->getMetadata();

        $builder
            ->select('en')
            ->from($class, 'en')
            ->orderBy('en.' . $meta->getSortBy(), $meta->getSortOrder())
        ;

        if ($meta->getPage() > 0) {
            $builder
                ->setFirstResult(($meta->getPage() - 1) * $meta->getLimit())
                ->setMaxResults($meta->getLimit())
            ;
        }

        $result = $builder->getQuery()->getResult();
        $list->setItems($result);
    }

    private function countTotal(
        InstanceList $list,
        EntityManagerInterface $manager,
        string $class
    ): void {
        $builder = $manager->createQueryBuilder();

        $builder
            ->select('count(en)')
            ->from($class, 'en')
        ;

        /** @noinspection PhpUnhandledExceptionInspection */
        $count = $builder->getQuery()->getSingleScalarResult();
        $meta = $list->getMetadata();
        $meta->setTotalCount($count);
    }
}

<?php

namespace App\Annotation\Parameter;

use Doctrine\Common\Annotations\Annotation\Target;
use FOS\RestBundle\Controller\Annotations\ParamInterface;
use FOS\RestBundle\Validator\Constraints\Regex;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Annotation
 * @Target("METHOD")
 */
class SortBy implements ParamInterface
{
    public function getName(): string
    {
        return 'sort-by';
    }

    public function getDefault(): int
    {
        return 'id';
    }

    public function getDescription(): string
    {
        return 'Defines the field against which list shall be sorted';
    }

    public function getIncompatibilities(): array
    {
        return [];
    }

    public function getConstraints(): array
    {
        $pattern = '/^[a-zA-Z_]+$/';
        return [new Regex($pattern)];
    }

    public function isStrict(): bool
    {
        return false;
    }

    public function getValue(Request $request, $default)
    {
        return $request->query->get('sort-by', $default);
    }
}

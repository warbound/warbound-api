<?php

namespace App\Annotation\Parameter;

use Doctrine\Common\Annotations\Annotation\Target;
use FOS\RestBundle\Controller\Annotations\ParamInterface;
use FOS\RestBundle\Validator\Constraints\Regex;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Annotation
 * @Target("METHOD")
 */
class Group implements ParamInterface
{
    private $enum;
    private $default;

    public function __construct(
        array $enum
    ) {
        if (!in_array('base', $this->enum, false)) {
            $this->enum[] = 'base';
        }

        $this->default = $this->enum[0];
    }

    public function getName(): string
    {
        return 'group';
    }

    public function getDefault(): string
    {
        return $this->default;
    }

    public function getDescription(): string
    {
        return 'Defines fields to be returned';
    }

    public function getIncompatibilities(): array
    {
        return [];
    }

    public function getConstraints(): array
    {
        $pattern = '/^(' . implode('|', $this->enum) . ')$/';
        return [new Regex($pattern)];
    }

    public function isStrict(): bool
    {
        return false;
    }

    public function getValue(Request $request, $default)
    {
        return $request->query->get('group', $default);
    }
}

<?php

namespace App\Annotation\Parameter;

use Doctrine\Common\Annotations\Annotation\Target;
use FOS\RestBundle\Controller\Annotations\ParamInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints\NotNull;

/**
 * @Annotation
 * @Target("METHOD")
 */
class Instance implements ParamInterface
{
    public function getName(): string
    {
        return 'instance';
    }

    public function getDefault()
    {
        return null;
    }

    public function getDescription(): string
    {
        return 'An instance in body.';
    }

    public function getIncompatibilities(): array
    {
        return [];
    }

    public function getConstraints(): array
    {
        return [new NotNull()];
    }

    public function isStrict(): bool
    {
        return false;
    }

    public function getValue(Request $request, $default)
    {
        return $request->getContent();
    }
}

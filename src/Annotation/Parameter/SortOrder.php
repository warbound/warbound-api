<?php

namespace App\Annotation\Parameter;

use Doctrine\Common\Annotations\Annotation\Target;
use FOS\RestBundle\Controller\Annotations\ParamInterface;
use FOS\RestBundle\Validator\Constraints\Regex;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Annotation
 * @Target("METHOD")
 */
class SortOrder implements ParamInterface
{
    public function getName(): string
    {
        return 'sort-order';
    }

    public function getDefault(): int
    {
        return 'desc';
    }

    public function getDescription(): string
    {
        return 'Defines the direction of the sort';
    }

    public function getIncompatibilities(): array
    {
        return [];
    }

    public function getConstraints(): array
    {
        $pattern = '/^(asc|desc)+$/';
        return [new Regex($pattern)];
    }

    public function isStrict(): bool
    {
        return false;
    }

    public function getValue(Request $request, $default)
    {
        return $request->query->get('sort-order', $default);
    }
}

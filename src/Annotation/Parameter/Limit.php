<?php

namespace App\Annotation\Parameter;

use Doctrine\Common\Annotations\Annotation\Target;
use FOS\RestBundle\Controller\Annotations\ParamInterface;
use FOS\RestBundle\Validator\Constraints\Regex;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Annotation
 * @Target("METHOD")
 */
class Limit implements ParamInterface
{
    public function getName(): string
    {
        return 'limit';
    }

    public function getDefault(): int
    {
        return '20';
    }

    public function getDescription(): string
    {
        return 'Defines list\'s page length';
    }

    public function getIncompatibilities(): array
    {
        return [];
    }

    public function getConstraints(): array
    {
        $pattern = '/^\d+$/';
        return [new Regex($pattern)];
    }

    public function isStrict(): bool
    {
        return false;
    }

    public function getValue(Request $request, $default)
    {
        return $request->query->get('limit', $default);
    }
}

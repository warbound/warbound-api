<?php

namespace App\Controller;

use App\Entity\Auth\Account;
use App\Util\Instantiator;
use App\Util\Lister;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\Controller\Annotations as Rest;
use App\Annotation\Parameter;

class AccountController extends BaseController
{
    /**
     * @Rest\Post("/accounts")
     *
     * @Parameter\Instance()
     *
     * @param Instantiator           $instantiator
     * @param EntityManagerInterface $manager
     *
     * @return Response
     */
    public function create(
        Instantiator $instantiator,
        EntityManagerInterface $manager
    ): Response {
        $account = $instantiator->bodyFetch(
            Account::class,
            Account::GROUP_CREATE
        );

        $manager->persist($account);
        $manager->flush();

        return $this->createView(
            $account,
            201,
            [Account::GROUP_CREATE_RESPONSE]
        );
    }

    /**
     * @Rest\Post("/accounts")
     *
     * @Parameter\Page()
     * @Parameter\Limit()
     * @Parameter\SortBy()
     * @Parameter\SortOrder()
     * @Parameter\Group()
     *
     * @param Lister $lister
     *
     * @return Response
     */
    public function list(
        Lister $lister
    ): Response {
        $list = $lister->list(Account::class);

        return $this->createView($list, 200);
    }
}

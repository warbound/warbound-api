<?php

namespace App\Controller;

use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Request\ParamFetcherInterface;
use Symfony\Component\HttpFoundation\Response;

abstract class BaseController extends AbstractFOSRestController
{
    private $paramFetcher;

    public function __construct(
        ParamFetcherInterface $paramFetcher
    ) {
        $this->paramFetcher = $paramFetcher;
    }

    protected function createView(
        ?object $object,
        int $statusCode = 200,
        array $groups = []
    ): Response {
        $view = $this->view($object, $statusCode);
        $view->getContext()->setGroups($groups);

        $paramGroup = $this->paramFetcher->get('group');
        if ($paramGroup && !in_array($paramGroup, $view->getContext()->getGroups(), true)) {
            $view->getContext()->addGroup($paramGroup);
        }

        return $this->handleView($view);
    }
}

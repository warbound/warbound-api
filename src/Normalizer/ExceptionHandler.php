<?php

namespace App\Normalizer;

use App\Exception\BadRequestHttpException;
use FOS\RestBundle\Serializer\Normalizer\AbstractExceptionNormalizer;
use Exception;
use JMS\Serializer\GraphNavigator;
use JMS\Serializer\Handler\SubscribingHandlerInterface;
use JMS\Serializer\JsonSerializationVisitor;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Validator\ConstraintViolationInterface;

class ExceptionHandler extends AbstractExceptionNormalizer implements SubscribingHandlerInterface
{
    private const FIELD_CODE = 'code';
    private const FIELD_ERROR = 'error';
    private const FIELD_PATH = 'path';
    private const FIELD_MESSAGE = 'message';
    private const FIELD_VIOLATIONS = 'violations';
    private const FIELD_TRACE = 'trace';

    public static function getSubscribingMethods(): array
    {
        return [
            [
                'direction' => GraphNavigator::DIRECTION_SERIALIZATION,
                'format' => 'json',
                'type' => Exception::class,
                'method' => 'serializeToJson',
                'priority' => -1
            ]
        ];
    }

    public function serializeToJson(
        JsonSerializationVisitor $visitor,
        Exception $exception
    ) {
        $response = [
            self::FIELD_CODE => ($exception instanceof HttpException) ? $exception->getStatusCode() : 500,
            self::FIELD_ERROR => [
                self::FIELD_MESSAGE => $exception->getMessage()
            ]
        ];

        if ($exception instanceof BadRequestHttpException) {
            /** @var ConstraintViolationInterface $violation */
            foreach ($exception->getConstraintViolationList() as $violation) {
                $response[self::FIELD_ERROR][self::FIELD_VIOLATIONS][] = [
                    self::FIELD_PATH => $violation->getPropertyPath(),
                    self::FIELD_MESSAGE => $violation->getMessage()
                ];
            }
        } elseif (!$exception instanceof HttpException) {
            $response[self::FIELD_ERROR][self::FIELD_TRACE] = $exception->getTrace();
        }

        return $visitor->prepare($response);
    }
}

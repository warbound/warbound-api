<?php

namespace App\Entity\Auth;

use App\Entity\Component\IdentifiableInterface;
use App\Entity\Component\IdentifiableTrait;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation as Serializer;

/**
 * @ORM\Entity
 * @ORM\Table("account")
 */
class Account implements IdentifiableInterface
{
    public const GROUP_CREATE = 'account-create';
    public const GROUP_CREATE_RESPONSE = 'account-create-response';

    use IdentifiableTrait;

    /**
     * @var string
     *
     * @Assert\NotBlank(
     *     message="USERNAME_CANNOT_BE_EMPTY",
     *     allowNull=false,
     *     groups={Account::GROUP_CREATE}
     * )
     * @Assert\Length(
     *     min="USERNAME_TOO_SHORT",
     *     maxMessage="USERNAME_TOO_LONG",
     *     min="2",
     *     max="32",
     *     groups={Account::GROUP_CREATE}
     * )
     *
     * @ORM\Column(
     *     type="string",
     *     length=32,
     *     unique=true
     * )
     *
     * @Serializer\Type("string")
     * @Serializer\Groups({
     *     Account::GROUP_CREATE,
     *     Account::GROUP_CREATE_RESPONSE
     * })
     */
    private $username;

    /**
     * @var string
     *
     * @ORM\Column(
     *     name="sha_pass_hash",
     *     type="string",
     *     length=40
     * )
     *
     * @Serializer\Type("string")
     * @Serializer\Groups({})
     */
    private $passwordHash;

    /**
     * @var string
     *
     * @Assert\NotBlank(
     *     message="PASSWORD_CANNOT_BE_EMPTY",
     *     allowNull=false,
     *     groups={Account::GROUP_CREATE}
     * )
     * @Assert\Regex(
     *     message="PASSWORD_TOO_WEAK",
     *     pattern="/^(?=.*[a-zA-Z])(?=.*\d).*$/",
     *     groups={Account::GROUP_CREATE}
     * )
     *
     * @Serializer\Type("string")
     * @Serializer\Groups({Account::GROUP_CREATE})
     */
    private $plainPassword;

    /**
     * @var string
     *
     * @ORM\Column(
     *     name="sessionkey",
     *     type="string",
     *     length=80,
     *     options={"default":""}
     * )
     *
     * @Serializer\Type("string")
     * @Serializer\Groups({})
     */
    private $sessionKey = '';

    /**
     * @var string
     *
     * @ORM\Column(
     *     type="string",
     *     length=64,
     *     options={"default":""}
     * )
     *
     * @Serializer\Type("string")
     * @Serializer\Groups({})
     */
    private $v = '';

    /**
     * @var string
     *
     * @ORM\Column(
     *     type="string",
     *     length=64,
     *     options={"default":""}
     * )
     *
     * @Serializer\Type("string")
     * @Serializer\Groups({})
     */
    private $s = '';

    /**
     * @var string
     *
     * @ORM\Column(
     *     type="string",
     *     length=100,
     *     options={"default":""}
     * )
     *
     * @Serializer\Type("string")
     * @Serializer\Groups({})
     */
    private $tokenKey = '';

    /**
     * @var string
     *
     * @Assert\NotBlank(
     *     message="EMAIL_CANNOT_BE_EMPTY",
     *     allowNull=false,
     *     groups={Account::GROUP_CREATE}
     * )
     * @Assert\Length(
     *     maxMessage="EMAIL_TOO_LONG",
     *     max="255",
     *     groups={Account::GROUP_CREATE}
     * )
     *
     * @ORM\Column(
     *     type="string",
     *     length=255
     * )
     *
     * @Serializer\Type("string")
     * @Serializer\Groups({
     *     Account::GROUP_CREATE,
     *     Account::GROUP_CREATE_RESPONSE
     * })
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(
     *     type="string",
     *     length=255
     * )
     *
     * @Serializer\Type("string")
     * @Serializer\Groups({})
     */
    private $regMail;

    /**
     * @var string
     *
     * @ORM\Column(
     *     name="joindate",
     *     type="datetime",
     *     options={"default":"CURRENT_TIMESTAMP"}
     * )
     * @ORM\Version
     *
     * @Serializer\Type("string")
     * @Serializer\Groups({})
     */
    private $joinDate;

    /**
     * @var string
     *
     * @ORM\Column(
     *     type="string",
     *     length=15,
     *     options={"default":"127.0.0.1"}
     * )
     *
     * @Serializer\Type("string")
     * @Serializer\Groups({})
     */
    private $lastIp = '127.0.0.1';

    /**
     * @var string
     *
     * @ORM\Column(
     *     type="string",
     *     length=15,
     *     options={"default":"127.0.0.1"}
     * )
     *
     * @Serializer\Type("string")
     * @Serializer\Groups({})
     */
    private $lastAttemptIp = '127.0.0.1';

    /**
     * @var int
     *
     * @ORM\Column(
     *     type="integer",
     *     length=10,
     *     options={"default":0}
     * )
     *
     * @Serializer\Type("integer")
     * @Serializer\Groups({})
     */
    private $failedLogins = 0;

    /**
     * @var int
     *
     * @ORM\Column(
     *     type="smallint",
     *     length=3,
     *     options={"default":0}
     * )
     *
     * @Serializer\Type("integer")
     * @Serializer\Groups({})
     */
    private $locked = 0;

    /**
     * @var string
     *
     * @ORM\Column(
     *     type="string",
     *     length=2,
     *     options={"default":"00"}
     * )
     *
     * @Serializer\Type("string")
     * @Serializer\Groups({})
     */
    private $lockCountry = '00';

    /**
     * @var string
     *
     * @ORM\Column(
     *     type="datetime",
     *     nullable=true
     * )
     * @ORM\Version
     *
     * @Serializer\Type("string")
     * @Serializer\Groups({})
     */
    private $lastLogin;

    /**
     * @var int
     *
     * @ORM\Column(
     *     type="integer",
     *     length=10,
     *     options={"default":0}
     * )
     *
     * @Serializer\Type("integer")
     * @Serializer\Groups({})
     */
    private $online = 0;

    /**
     * @var int
     *
     * @ORM\Column(
     *     type="smallint",
     *     length=3,
     *     options={"default":2}
     * )
     *
     * @Serializer\Type("integer")
     * @Serializer\Groups({})
     */
    private $expansion = 2;

    /**
     * @var int
     *
     * @ORM\Column(
     *     name="mutetime",
     *     type="bigint",
     *     length=20,
     *     options={"default":0}
     * )
     *
     * @Serializer\Type("integer")
     * @Serializer\Groups({})
     */
    private $muteTime = 0;

    /**
     * @var string
     *
     * @ORM\Column(
     *     name="mutereason",
     *     type="string",
     *     length=255,
     *     options={"default":""}
     * )
     *
     * @Serializer\Type("string")
     * @Serializer\Groups({})
     */
    private $muteReason = '';

    /**
     * @var string
     *
     * @ORM\Column(
     *     type="string",
     *     length=50,
     *     options={"default":""}
     * )
     *
     * @Serializer\Type("string")
     * @Serializer\Groups({})
     */
    private $muteby = '';

    /**
     * @var int
     *
     * @ORM\Column(
     *     type="smallint",
     *     length=3,
     *     options={"default":0}
     * )
     *
     * @Serializer\Type("integer")
     * @Serializer\Groups({})
     */
    private $locale = 0;

    /**
     * @var string
     *
     * @ORM\Column(
     *     type="string",
     *     length=3,
     *     options={"default":""}
     * )
     *
     * @Serializer\Type("string")
     * @Serializer\Groups({})
     */
    private $os = '';

    /**
     * @var int
     *
     * @ORM\Column(
     *     type="integer",
     *     length=10,
     *     options={"default":0}
     * )
     *
     * @Serializer\Type("integer")
     * @Serializer\Groups({})
     */
    private $recruiter = 0;

    /**
     * @var int
     *
     * @ORM\Column(
     *     name="totaltime",
     *     type="integer",
     *     length=10,
     *     options={"default":0}
     * )
     *
     * @Serializer\Type("integer")
     * @Serializer\Groups({})
     */
    private $totalPlaytime = 0;

    public function getUsername(): string
    {
        return $this->username;
    }

    public function setUsername(string $username): Account
    {
        $this->username = $username;

        return $this;
    }

    public function getPasswordHash(): string
    {
        return $this->passwordHash;
    }

    public function setPasswordHash(string $passwordHash): Account
    {
        $this->passwordHash = $passwordHash;

        return $this;
    }

    public function getPlainPassword(): string
    {
        return $this->plainPassword;
    }

    public function setPlainPassword(string $plainPassword): Account
    {
        $this->plainPassword = $plainPassword;

        return $this;
    }

    public function getSessionKey(): string
    {
        return $this->sessionKey;
    }

    public function setSessionKey(string $sessionKey): Account
    {
        $this->sessionKey = $sessionKey;

        return $this;
    }

    public function getV(): string
    {
        return $this->v;
    }

    public function setV(string $v): Account
    {
        $this->v = $v;

        return $this;
    }

    public function getS(): string
    {
        return $this->s;
    }

    public function setS(string $s): Account
    {
        $this->s = $s;

        return $this;
    }

    public function getTokenKey(): string
    {
        return $this->tokenKey;
    }

    public function setTokenKey(string $tokenKey): Account
    {
        $this->tokenKey = $tokenKey;

        return $this;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function setEmail(string $email): Account
    {
        $this->email = $email;

        return $this;
    }

    public function getRegMail(): string
    {
        return $this->regMail;
    }

    public function setRegMail(string $regMail): Account
    {
        $this->regMail = $regMail;

        return $this;
    }

    public function getJoinDate(): string
    {
        return $this->joinDate;
    }

    public function setJoinDate(string $joinDate): Account
    {
        $this->joinDate = $joinDate;

        return $this;
    }

    public function getLastIp(): string
    {
        return $this->lastIp;
    }

    public function setLastIp(string $lastIp): Account
    {
        $this->lastIp = $lastIp;

        return $this;
    }

    public function getLastAttemptIp(): string
    {
        return $this->lastAttemptIp;
    }

    public function setLastAttemptIp(string $lastAttemptIp): Account
    {
        $this->lastAttemptIp = $lastAttemptIp;

        return $this;
    }

    public function getFailedLogins(): int
    {
        return $this->failedLogins;
    }

    public function setFailedLogins(int $failedLogins): Account
    {
        $this->failedLogins = $failedLogins;

        return $this;
    }

    public function getLocked(): int
    {
        return $this->locked;
    }

    public function setLocked(int $locked): Account
    {
        $this->locked = $locked;

        return $this;
    }

    public function getLockCountry(): string
    {
        return $this->lockCountry;
    }

    public function setLockCountry(string $lockCountry): Account
    {
        $this->lockCountry = $lockCountry;

        return $this;
    }

    public function getLastLogin(): string
    {
        return $this->lastLogin;
    }

    public function setLastLogin(string $lastLogin): Account
    {
        $this->lastLogin = $lastLogin;

        return $this;
    }

    public function getOnline(): int
    {
        return $this->online;
    }

    public function setOnline(int $online): Account
    {
        $this->online = $online;

        return $this;
    }

    public function getExpansion(): string
    {
        return $this->expansion;
    }

    public function setExpansion(string $expansion): Account
    {
        $this->expansion = $expansion;

        return $this;
    }

    public function getMuteTime(): int
    {
        return $this->muteTime;
    }

    public function setMuteTime(int $time): Account
    {
        $this->muteTime = $time;

        return $this;
    }

    public function getMuteReason(): string
    {
        return $this->muteReason;
    }

    public function setMuteReason(string $reason): Account
    {
        $this->muteReason = $reason;

        return $this;
    }

    public function getMuteby(): string
    {
        return $this->muteby;
    }

    public function setMuteby(string $muteby): Account
    {
        $this->muteby = $muteby;

        return $this;
    }

    public function getLocale(): int
    {
        return $this->locale;
    }

    public function setLocale(int $locale): Account
    {
        $this->locale = $locale;

        return $this;
    }

    public function getOs(): string
    {
        return $this->os;
    }

    public function setOs(string $os): Account
    {
        $this->os = $os;

        return $this;
    }

    public function getRecruiter(): int
    {
        return $this->recruiter;
    }

    public function setRecruiter(int $recruiter): Account
    {
        $this->recruiter = $recruiter;

        return $this;
    }

    public function getTotalPlaytime(): int
    {
        return $this->totalPlaytime;
    }

    public function setTotalPlaytime(int $time): Account
    {
        $this->totalPlaytime = $time;

        return $this;
    }
}

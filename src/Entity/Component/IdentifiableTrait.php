<?php

namespace App\Entity\Component;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;

trait IdentifiableTrait
{
    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue()
     *
     * @Serializer\Type("integer")
     * @Serializer\Groups("base")
     * @Serializer\ReadOnly()
     */
    protected $id;

    public function getId(): ?int
    {
        return $this->id;
    }
}

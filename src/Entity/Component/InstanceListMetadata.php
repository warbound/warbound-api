<?php

namespace App\Entity\Component;

use JMS\Serializer\Annotation as Serializer;

class InstanceListMetadata
{
    /**
     * @var int
     *
     * @Serializer\Type("integer")
     * @Serializer\Groups({"base"})
     */
    private $page;

    /**
     * @var int
     *
     * @Serializer\Type("integer")
     * @Serializer\Groups({"base"})
     */
    private $limit;

    /**
     * @var int
     *
     * @Serializer\Type("integer")
     * @Serializer\Groups({"base"})
     */
    private $totalCount;

    private $sortBy;
    private $sortOrder;

    public function __construct()
    {
        $this->page = 0;
        $this->limit = 20;
        $this->totalCount = 0;
    }

    public function getPage(): int
    {
        return $this->page;
    }

    public function setPage(int $page): self
    {
        $this->page = $page;

        return $this;
    }

    public function getLimit(): int
    {
        return $this->limit;
    }

    public function setLimit(int $limit): self
    {
        $this->limit = $limit;

        return $this;
    }

    public function getTotalCount(): int
    {
        return $this->totalCount;
    }

    public function setTotalCount(int $count): self
    {
        $this->totalCount = $count;

        return $this;
    }

    public function getSortBy(): string
    {
        return $this->sortBy;
    }

    public function setSortBy(string $field): self
    {
        $this->sortBy = $field;

        return $this;
    }

    public function getSortOrder(): string
    {
        return $this->sortOrder;
    }

    public function setSortOrder(string $order): self
    {
        $this->sortOrder = $order;

        return $this;
    }

    /**
     * @Serializer\SerializedName("page_count")
     * @Serializer\Type("integer")
     * @Serializer\Groups({"base"})
     *
     * @return int
     */
    public function getPageCount(): int
    {
        return $this->page !== 0 ? ceil($this->totalCount / $this->limit) : 1;
    }
}

<?php

namespace App\Entity\Component;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;
use DateTime;

trait TimestampableTrait
{
    /**
     * @var DateTime | null
     *
     * @ORM\Column(type="datetime")
     *
     * @Serializer\Type("DateTime")
     * @Serializer\Groups("timestamp")
     */
    protected $createdAt;

    /**
     * @var DateTime | null
     *
     * @ORM\Column(type="datetime")
     *
     * @Serializer\Type("DateTime")
     * @Serializer\Groups("timestamp")
     */
    protected $modifiedAt;

    public function getCreatedAt(): ?DateTime
    {
        return $this->createdAt;
    }

    public function setCreatedAt(?DateTime $createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getModifiedAt(): ?DateTime
    {
        return $this->modifiedAt;
    }

    public function setModifiedAt(?DateTime $modifiedAt)
    {
        $this->modifiedAt = $modifiedAt;

        return $this;
    }

    /**
     * @ORM\PrePersist()
     */
    public function prePersist(): void
    {
        $this->createdAt = new DateTime();
        $this->modifiedAt = new DateTime();
    }

    /**
     * @ORM\PreUpdate()
     */
    public function preUpdate(): void
    {
        $this->modifiedAt = new DateTime();
    }
}

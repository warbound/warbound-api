<?php

namespace App\Entity\Component;

use FOS\RestBundle\Request\ParamFetcherInterface;
use JMS\Serializer\Annotation as Serializer;

class InstanceList
{
    /**
     * @var array
     *
     * @Serializer\Type("array")
     * @Serializer\Groups({"base"})
     */
    private $items;

    /**
     * @var InstanceListMetadata
     *
     * @Serializer\Type("App\Entity\Component\InstanceListMetadata")
     * @Serializer\Groups({"base"})
     */
    private $metadata;

    public function __construct()
    {
        $this->items = [];
        $this->metadata = new InstanceListMetadata();
    }

    public function setItems(array $items): self
    {
        $this->items = $items;

        return $this;
    }

    public function getItems(): array
    {
        return $this->items;
    }

    public function getMetadata(): InstanceListMetadata
    {
        return $this->metadata;
    }
}

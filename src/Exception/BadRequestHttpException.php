<?php

namespace App\Exception;

use Symfony\Component\HttpKernel\Exception\BadRequestHttpException as CoreBadRequestHttpException;
use Symfony\Component\Validator\ConstraintViolationListInterface;
use Throwable;

class BadRequestHttpException extends CoreBadRequestHttpException
{
    private $constraintViolationList;

    public function __construct(
        ConstraintViolationListInterface $list,
        string $message = null,
        Throwable $previous = null,
        int $code = 0,
        array $headers = []
    ) {
        $this->constraintViolationList = $list;

        parent::__construct($message ?? 'BAD_REQUEST', $previous, $code, $headers);
    }

    public function getConstraintViolationList(): ConstraintViolationListInterface
    {
        return $this->constraintViolationList;
    }
}

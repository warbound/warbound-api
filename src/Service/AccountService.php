<?php

namespace App\Service;

use App\Entity\Auth\Account;
use Doctrine\Persistence\ManagerRegistry;

class AccountService
{
    private $manager;

    public function __construct(ManagerRegistry $registry)
    {
        $this->manager = $registry->getManagerForClass(Account::class);
    }

    public function persist(Account $account): void
    {
        $hashedPassword = sha1(strtoupper($account->getUsername()) . ':' . strtoupper($account->getPlainPassword()));
        $account
            ->setRegMail($account->getEmail())
            ->setPasswordHash($hashedPassword)
        ;

        $this->manager->persist($account);
        $this->manager->flush();
    }
}
